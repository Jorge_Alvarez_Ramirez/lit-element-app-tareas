import{LitElement, html, css} from 'lit-element';

export class CheckElement extends LitElement{
    static get styles(){
        return css`
            .check {
            width:30px;
            height:30px;
            }
        `;
    }

    _actionChecked(){
        console.log('ha sido selecionado' , this.isChecked);
    }
    render(){
        return html`
            <input class="check" type="checkbox" @click="${this._actionChecked}">
        `;
    }
}

customElements.define('check-element', CheckElement);