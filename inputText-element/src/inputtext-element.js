import {LitElement, css, html} from 'lit-element';

export class InputTextElement extends LitElement{
    static get styles(){
        return css`
            input{
                border-radius: 4px;
                color: #2E3133;
                width:300px
            }
        `;
    }

    static get properties(){
        return{
            valorInput: {type: String},
            placeHolder :{type: String}
        }
    }

    constructor(){
        super();
        this.valorInput =  '';
        this.placeHolder = 'Escriba su tarea...'
    }

    _getValue(e){
        this.valorInput = e.target.value;
    }

    render(){
        return html`
            <input type="text" @input="${this._getValue}" placeholder="${this.placeHolder}">
        `;
    }
}

customElements.define('inputtext-element', InputTextElement);