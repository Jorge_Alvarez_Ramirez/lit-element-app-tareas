import {LitElement, css, html} from 'lit-element';
import './boton-element'

export class InputTextElement extends LitElement{
    static get styles(){
        return css`
            input{
                border-radius: 4px;
                color: #2E3133;
                width:300px
            }
        `;
    }

    static get properties(){
        return{
            valorInput: {type: String},
            placeHolder :{type: String}
        }
    }

    constructor(){
        super();
        this.valorInput =  '';
        this.placeHolder = '';
    }


    _getValue(e){
        this.valorInput = e.target.value;
    }

    _eventBoton(){
        if(this.valorInput === ''){
            alert('Debe ingresar un deseo para añadirlo.');
        }
        else{
            this.dispatchEvent(new CustomEvent('envioTexto',{
                bubbles: true,
                composed: true,
                detail: this.valorInput
            }));
            this.shadowRoot.querySelector('input').value="";
        }
    }

    render(){
        return html`
            <input type="text" @input="${this._getValue}" placeholder="${this.placeHolder}">
            <boton-element valueButton="Añadir" @eventboton="${this._eventBoton}"></boton-element>
        `;
    }
}

customElements.define('inputtext-element', InputTextElement);