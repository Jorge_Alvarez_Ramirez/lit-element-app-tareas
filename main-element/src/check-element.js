import{LitElement, html, css} from 'lit-element';

export class CheckElement extends LitElement{
    static get styles(){
        return css`
            input {
            width:20px;
            height:20px;
            }
            p{
                font-size:20px;
            }
            .completa{
                text-decoration: line-through;
            }
            .aTiempo{
                color: black
            }
            .aVerde{
                color: green;
            }
            .aAmarillo{
                color: yellow;
            }
            .aRojo{
                color: red;
            }
        `;
    }

    static get properties(){
        return{
            textoMostrar:{type: String},
            valueCheck:{type: Number},
            isChecked: {type: Boolean},
            claseTiempo: {type: String}
        }
    }

    constructor(){
        super();
        this.textoMostrar = "";
        this.valueCheck= null;
        this.isChecked= false;
        this.claseTiempo='aTiempo'
    }

    _actionChecked(){
        this.isChecked = !this.isChecked;
    }

    connectedCallback() {
        super.connectedCallback();
        setTimeout(() => {
            this.claseTiempo='aVerde';
          }, 3000);
          setTimeout(() =>{
            this.claseTiempo='aAmarillo';
          }, 5000);
          setTimeout(() => {
            this.claseTiempo='aRojo';
          }, 7000);  
    }

    render(){
        return html`
            <div class="${this.claseTiempo}">
                <p class="${this.isChecked ? "completa": "pendiente"}">
                    <input .checked="${this.isChecked}" type="checkbox" @change="${this._actionChecked}" value="${this.valueCheck}">
                    ${this.textoMostrar}
                </p>
            </div>
        `;
    }
}

customElements.define('check-element', CheckElement);