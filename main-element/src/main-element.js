import {LitElement, html, css} from 'lit-element';
import './boton-element'
import './check-element'
import './inputtext-element'

export class MainElement extends LitElement{
    static get styles(){
        return css`

            .container-de-deseo{
                display: flex;
                flex-direction: row;
            }
            legend{
                font-size: 18px;
            }
            .contenedor-principal{
                display: flex;
                justify-content: center;
                align-items: center;
                flex-direction: column;
            }
        `;
    }

    static get properties(){
        return{
            listaDeseos:{type: Array}
        }
    }

    constructor(){
        super();
        this.listaDeseos= [];
    }


    _añadirNuevo(e){
        const deseo ={
            id: this.listaDeseos.length>0 ? this.listaDeseos[this.listaDeseos.length -1].id + 1 : 1,
            deseo: e.detail,
            complete: false
        }
        this.listaDeseos.push(deseo);
        this.requestUpdate();
    }

    render(){
        return html`
        <div class="contenedor-principal">
            <h1>Lista de deseos</h1>
            <fieldset>
                <legend>Nuevo deseo</legend>
                <div class="campo-añadir-deseo">
                    <inputtext-element placeHolder="Escriba su deseo..." @envioTexto="${this._añadirNuevo}"></inputtext-element>
                </div>
            </fieldset>
            <div class="campo-lista-de-deseos">
                ${this.listaDeseos.map(deseo => { return html `
                <div class="container-de-deseo">
                    <check-element .isChecked="${deseo.complete}" textoMostrar="${deseo.deseo}" valueCheck="${deseo.id}" @event-check="${this._rayarTexto}"></check-element>
                </div>
                `})}
            </div>
        </div>
        `;
    }
}

customElements.define('main-element', MainElement );