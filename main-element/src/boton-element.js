import { LitElement ,html , css} from 'lit-element';

export class ButtonElment extends LitElement{
    static get styles(){
        return css`
            button{
                background-color: #34c6eb;
                color: black;
                cursor:pointer;
                width:100px;
                height: 22px;
                font-family: sans-serif, Times;
                border-radius:5px;
            }

        `;
    }

    static get properties(){
        return{
            valueButton:{type: String}
        }
    }

    constructor(){
        super();
        this.valueButton = 'Enviar';
    }

    _EventoButton(){
        this.dispatchEvent(new CustomEvent('eventboton',{
            bubbles: true,
            composed: true,
            detail: 'has algo'
        }));
    }

    render(){
        return html`
            <button @click="${this._EventoButton}">${this.valueButton}</button>
        `;
    }
}

customElements.define('boton-element', ButtonElment);