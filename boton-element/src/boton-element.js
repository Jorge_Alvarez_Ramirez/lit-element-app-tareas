import { LitElement ,html , css} from 'lit-element';

export class ButtonElment extends LitElement{
    static get styles(){
        return css`
            button{
                background-color: #34c6eb;
                color: black;
                cursor:pointer;
                width:100px;
                height: 35px;
                font-family: sans-serif, Times;
                font-size: 18px;
                border-radius:5px;
            }

        `;
    }

    static get properties(){
        return{
            valueButton:{type: String}
        }
    }

    constructor(){
        super();
        this.valueButton = 'Enviar';
    }

    _EventoButton(){
        console.log("se ha echo click")
    }

    render(){
        return html`
            <button @click="${this._EventoButton}" type="button">${this.valueButton}</button>
        `;
    }
}

customElements.define('boton-element', ButtonElment);